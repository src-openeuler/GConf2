Name:           GConf2
Version:        3.2.6
Release:        28
Summary:        GConf is a system for storing application preferences
License:        LGPL-2.0-or-later and GPL-2.0-or-later
URL:            https://gitlab.gnome.org/Archive/gconf/
Source0:        https://download.gnome.org/sources/GConf/3.2/GConf-%{version}.tar.xz
Source1:        macros.gconf2

#PATCH-FIX-https://bugzilla.gnome.org/show_bug.cgi?id=671490
Patch0001:      drop-spew.patch
Patch0099:      workaround-crash.patch
#PATCH-FIX-upstream-https://patchwork.kernel.org/patch/10583361/
Patch0100:      pkill-hack.patch
BuildRequires: gcc make
BuildRequires: autoconf automake libtool
BuildRequires: pkgconfig(dbus-1) >= 1.0.0
BuildRequires: pkgconfig(dbus-glib-1) >= 0.74
BuildRequires: pkgconfig(gio-2.0) >= 2.31.0
BuildRequires: pkgconfig(glib-2.0) > 2.14.0
BuildRequires: pkgconfig(gmodule-2.0) >= 2.7.0
BuildRequires: pkgconfig(gobject-2.0) >= 2.7.0
BuildRequires: pkgconfig(gthread-2.0)
BuildRequires: pkgconfig(gtk+-3.0) >= 2.90
BuildRequires: pkgconfig(libxml-2.0)
BuildRequires: pkgconfig(polkit-gobject-1)
BuildRequires: gobject-introspection-devel >= 0.9.5
BuildRequires: gtk-doc >= 1.0
BuildRequires: intltool >= 0.35.0
Requires:       dbus procps-ng
Conflicts:      GConf2-dbus

Provides:       GConf2-gtk = 3.2.6-6
Obsoletes:      GConf2-gtk < 3.2.6-6

%description
The root motivation for GConf is to make application preferences more manageable for system administrators.
The initial GConf implementation has some shortcomings in this area; however, the client-side API should be correct,
and the initial implementation is a decent first cut GConf involves a few new concepts,
but no rocket science. Essentially GConf provides a preferences database, which is like a simple filesystem

%package        devel
Summary:        Development files for GConf2 package
Requires:       %{name} = %{version}-%{release}
Conflicts:      GConf2-dbus-devel

%description    devel
GConf2-devel package Contains library files needed for doing development.
Essentially GConf provides a preferences database, which is like a simple filesystem

%prep
%autosetup -n GConf-%{version} -p1

%build
autoreconf -fi
%configure --disable-static \
    --enable-defaults-service \
    --disable-orbit \
    --with-gtk=3.0 \
    --without-openldap
%make_build

%install
%make_install
%delete_la
install -d $RPM_BUILD_ROOT%{_sysconfdir}/gconf/{schemas,gconf.xml.system}
install -d $RPM_BUILD_ROOT%{_rpmconfigdir}/macros.d/
install -d $RPM_BUILD_ROOT%{_localstatedir}/lib/rpm-state/gconf
install -d $RPM_BUILD_ROOT%{_datadir}/GConf/gsettings
install -p -m 644 %{S:1} $RPM_BUILD_ROOT%{_rpmconfigdir}/macros.d/

%find_lang %name

%post
if [ $1 -gt 1 ]; then
    if ! fgrep -q gconf.xml.system %{_sysconfdir}/gconf/2/path; then
        sed -i -e 's@xml:readwrite:$(HOME)/.gconf@&\n\n# Location for system-wide settings.\nxml:readonly:/etc/gconf/gconf.xml.system@' %{_sysconfdir}/gconf/2/path
    fi
fi

%files -f %{name}.lang
%license COPYING
%doc NEWS README
%config(noreplace) %{_sysconfdir}/gconf/2/path
%dir %{_sysconfdir}/gconf/2
%dir %{_sysconfdir}/gconf/gconf.xml.defaults
%dir %{_sysconfdir}/gconf/gconf.xml.mandatory
%dir %{_sysconfdir}/gconf/gconf.xml.system
%dir %{_sysconfdir}/gconf/schemas
%{_bindir}/gconf*
%{_bindir}/gsettings-data-convert
%{_sysconfdir}/xdg/autostart/gsettings-data-convert.desktop
%{_libexecdir}/gconfd-2
%{_libdir}/*.so.*
%{_libdir}/GConf/2/*.so
%{_datadir}/sgml/gconf
%{_datadir}/GConf
%{_mandir}/man1/*
%exclude %{_mandir}/man1/gsettings-schema-convert.1*
%{_rpmconfigdir}/macros.d/macros.gconf2
%{_sysconfdir}/dbus-1/system.d/org.gnome.GConf.Defaults.conf
%{_libexecdir}/gconf-defaults-mechanism
%{_datadir}/polkit-1/actions/org.gnome.gconf.defaults.policy
%{_datadir}/dbus-1/system-services/org.gnome.GConf.Defaults.service
%{_datadir}/dbus-1/services/org.gnome.GConf.service
%{_localstatedir}/lib/rpm-state/gconf/
%{_libdir}/gio/modules/libgsettingsgconfbackend.so
%{_libdir}/girepository-1.0/*

%files devel
%{_libdir}/*.so
%{_includedir}/gconf
%{_datadir}/aclocal/*.m4
%{_datadir}/gtk-doc/html/gconf
%{_libdir}/pkgconfig/*
%{_datadir}/gir-1.0/*
%{_bindir}/gsettings-schema-convert
%{_mandir}/man1/gsettings-schema-convert.1*

%changelog
* Tue Sep 24 2024 Funda Wang <fundawang@yeah.net> - 3.2.6-28
- cleanup spec

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 3.2.6-27
- fix changelog

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 3.2.6-26
- delete gdb in buildrequires

* Mon May 31 2021 hanhui <hanhui15@huawei.com> - 3.2.6-25
- modify install require

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 3.2.6-24
- exclude the evoldap.conf

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.2.6-23
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: delete patches

* Wed Sep 18 2019 openEuler jimmy<dukaitian@huawei.com>  - 3.2.6-22
- Package init jimmy
